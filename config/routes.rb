Rails.application.routes.draw do
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "videos#index"
  resources :videos, only: [:index, :new, :create]
  resources :votes, only: [:index] do
    post :up_vote, on: :member, defaults: { format: :js }
    post :down_vote, on: :member, defaults: { format: :js }
  end
end
