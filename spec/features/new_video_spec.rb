require 'rails_helper'

describe 'Check list videos' do
  let(:user) { create(:user) }

  context 'user logged in' do
    before do
      sign_in user
      visit new_video_path
    end

    it 'check display' do
      expect(page).to have_content("Share a youtube movie")
      expect(page).to have_selector("input")
    end

    it 'fill video url valid' do
      within("#new_video") do
        fill_in 'Url', with: 'https://youtu.be/HROrHLrC61k'
      end
      click_button 'Share'
      expect(page).to have_content 'Share success'
    end

    it 'fill video url blank' do
      within("#new_video") do
        fill_in 'Url', with: ''
      end
      click_button 'Share'
      expect(page).to have_content "Url can't be blank"
    end

    it 'fill video url invalid' do
      within("#new_video") do
        fill_in 'Url', with: 'http://localhost:3010/videos'
      end
      click_button 'Share'
      expect(page).to have_content "Please enter the correct youtube link"
    end
  end
  context 'user is visitor' do
    before do
      visit new_video_path
    end

    it 'check display' do
      expect(page).not_to have_current_path(new_video_path)
    end
  end
end
