require 'rails_helper'

describe 'Check layout' do
  let(:user) { build(:user) }
  context 'when user login' do
    before do
      sign_in user
      visit root_path
    end
    it { expect(page).to have_content(user.email) }
    it { expect(page).to have_content('Share a movie') }
    it { expect(page).to have_content('Logout') }
  end

  context 'when user is visitor' do
    before do
      visit root_path
    end
    it { expect(page).to have_content("Login") }
    it { expect(page).to have_content('Signin') }
  end
end
