require 'rails_helper'

describe 'Check list videos' do
  let(:user) { create(:user) }
  let!(:video) { create(:video, user_id: user.id) }

  context 'when user logged in' do
    before do
      sign_in user
      visit root_path
    end

    it 'check display' do
      expect(page).to have_content("Ý thức bảo vệ chủ quyền biển đảo của dân tộc | VTV24")
      expect(page).to have_css(".vote-#{video.id}")
    end

    it 'check click upvote' do
      find(".vote-#{video.id}").first("button").click
      expect(user.votes.first.type_vote).to eq("up_vote")
    end

    it 'check click down vote' do
      find(".vote-#{video.id}").all("button").last.click
      expect(user.votes.first.type_vote).to eq("down_vote")
    end
  end
  context 'when user is visitor' do
    before do
      visit root_path
    end

    it 'check display' do
      expect(page).to have_content("Ý thức bảo vệ chủ quyền biển đảo của dân tộc | VTV24")
      expect(page).not_to have_css(".vote-#{video.id}")
    end
  end
end
