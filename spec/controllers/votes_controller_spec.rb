require 'rails_helper'

describe VotesController, type: :controller do
  let(:user) { create(:user) }
  let(:video) { create(:video, user_id: user.id) }

  describe 'POST upvote' do
    context 'when user logged in' do
      before do
        login_by(user)
      end

      context 'when voted' do
        before do
          create(:vote, user_id: user.id, video_id: video.id, type_vote: :up_vote)
          post :up_vote, params: { id: video.id }, format: :js
        end
        it { expect(assigns(:video_up_vote_ids)).to eq [] }
      end


      context "when haven't voted yet" do
        before { post :up_vote, params: { id: video.id }, format: :js }
        it { expect(assigns(:vote).type_vote).to eq "up_vote" }
        it { expect(assigns(:video_up_vote_ids)).to eq [video.id] }
      end
    end

    context 'when user is visitor' do
      before do
        post :up_vote, params: { id: video.id }, format: :js
      end

      it { expect(response).to have_http_status(401) }
    end
  end

  describe 'POST down vote' do
    context 'when user logged in' do
      before do
        login_by(user)
      end

      context 'when down voted' do
        before do
          create(:vote, user_id: user.id, video_id: video.id, type_vote: :down_vote)
          post :down_vote, params: { id: video.id }, format: :js
        end
        it { expect(assigns(:video_down_vote_ids)).to eq [] }
      end


      context "when haven't down voted yet" do
        before { post :down_vote, params: { id: video.id }, format: :js }
        it { expect(assigns(:vote).type_vote).to eq "down_vote" }
        it { expect(assigns(:video_down_vote_ids)).to eq [video.id] }
      end
    end

    context 'when user is visitor' do
      before do
        post :down_vote, params: { id: video.id }, format: :js
      end

      it { expect(response).to have_http_status(401) }
    end
  end
end
