require 'rails_helper'

describe VideosController, type: :controller do
  let(:user) { create(:user) }
  let(:video1) { create(:video, user_id: user.id) }
  let(:video2) { create(:video, user_id: user.id) }

  describe 'GET index' do
    context 'when user logged in' do
      before do
        login_by(user)
        create(:vote, user_id: user.id, video_id: video1.id, type_vote: :up_vote)
        create(:vote, user_id: user.id, video_id: video2.id, type_vote: :down_vote)
        get :index
      end

      it { expect(assigns(:videos)).not_to be_nil }
      it { expect(assigns(:videos)).to eq [video2, video1] }
      it { expect(assigns(:video_up_vote_ids)).to eq [video1.id] }
      it { expect(assigns(:video_down_vote_ids)).to eq [video2.id] }
    end

    context 'when user is visitor' do
      before do
        create(:vote, user_id: user.id, video_id: video1.id, type_vote: :up_vote)
        create(:vote, user_id: user.id, video_id: video2.id, type_vote: :down_vote)
        get :index
      end

      it { expect(assigns(:videos)).not_to be_nil }
      it { expect(assigns(:videos)).to eq [video2, video1] }
      it { expect(assigns(:video_up_vote_ids)).to be_nil }
      it { expect(assigns(:video_down_vote_ids)).to be_nil }
    end
  end

  describe 'GET new' do
    context 'when user logged in' do
      before do
        login_by(user)
        get :new
      end

      it { expect(response).to have_http_status(:success) }
    end

    context 'when user is visitor' do
      before do
        get :new
      end

      it { expect(response).to have_http_status(302) }
    end
  end

  describe 'POST create' do
    context 'when user logged in' do
      context 'when save success' do
        before do
          login_by(user)
          post :create, params: { video: { url: 'https://www.youtube.com/watch?v=FbsO3bgyH84&ab_channel=catholiclofi' } }
        end

        it { expect(flash[:success]).to eq 'Share success' }
      end

      context 'when save fail' do
        before do
          login_by(user)
          post :create, params: { video: { url: '' } }
        end

        it { expect(flash[:error].first).to eq "Url can't be blank" }
      end
    end

    context 'when user is visitor' do
      before do
        post :create
      end

      it { expect(response).to have_http_status(302) }
    end
  end
end
