FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n + 1}@example.com" }
    password "123456"
  end
end
