FactoryGirl.define do
  factory :video do
    sequence(:url){ |n| "https://www.youtube.com/watch?v=zbpLdet3vfQ&ab_channel=VTV24&stt=#{n + 1}" }
  end
end
