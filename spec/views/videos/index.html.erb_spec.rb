# -*- coding: utf-8 -*-
require 'rails_helper'

describe "videos/index", type: :view do
  let(:user) { create(:user) }
  before do
    assign(:videos, [stub_model(Video, { user_id: user.id, url: 'https://www.youtube.com/watch?v=Av5CshqGfSs&ab_channel=BBSharkLyrics' }),
                     stub_model(Video, { user_id: user.id, url: 'https://www.youtube.com/watch?v=Av5CshqGfSs' })])
  end

  context 'when logged in' do
    before do
      login_by(user)
      assign(:video_up_vote_ids, [])
      assign(:video_down_vote_ids, [])
      render
    end
    it { expect(view).to render_template(:partial => "_video", :count => 2) }
    it { expect(rendered).to match user.email }
    it { expect(rendered).to match /fa-thumbs-o-up/ }
  end

  context 'when user is visitor' do
    before do
      render
    end
    it { expect(view).to render_template(:partial => "_video", :count => 2) }
    it { expect(rendered).to match user.email }
    it { expect(rendered).not_to match /fa-thumbs-o-up/ }
  end
end
