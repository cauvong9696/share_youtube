# -*- coding: utf-8 -*-
require 'rails_helper'

describe "videos/new", type: :view do
  let(:user) { create(:user) }
  before do
    assign(:video, stub_model(Video, { user_id: user.id, url: 'https://www.youtube.com/watch?v=Av5CshqGfSs' }))
  end

    before do
      login_by(user)
      render
    end
    it { expect(rendered).to match /Share a youtube movie/ }
    it { expect(rendered).to match /Url/ }
end
