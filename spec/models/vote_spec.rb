require 'rails_helper'

describe Vote, type: :model do
  let(:user) { create(:user) }
  let(:video1) { create(:video, user_id: user.id) }
  let(:video2) { create(:video, user_id: user.id) }
  let(:vote1) { create(:vote, user_id: user.id, video_id: video1.id, type_vote: :up_vote) }
  let(:vote2) { create(:vote, user_id: user.id, video_id: video2.id, type_vote: :down_vote) }

  it { expect(vote1.user).to eq user }
  it { expect(vote1.video).to eq video1 }
  it { expect(vote1.type_vote).to eq "up_vote" }
  it { expect(vote2.type_vote).to eq "down_vote" }
end
