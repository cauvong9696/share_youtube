require 'rails_helper'

describe Video, type: :model do
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }
  let(:video) { create(:video, user_id: user1.id) }

  before do
    create(:vote, user_id: user1.id, video_id: video.id, type_vote: :up_vote)
    create(:vote, user_id: user2.id, video_id: video.id, type_vote: :down_vote)
  end

  it { expect(video.user).to eq user1 }
  it { expect(video.votes.size).to eq 2 }
  it { expect(video.up_votes.size).to eq 1 }
  it { expect(video.up_votes.first.type_vote).to eq 'up_vote' }
  it { expect(video.down_votes.size).to eq 1 }
  it { expect(video.down_votes.first.type_vote).to eq 'down_vote' }
  it { expect(video.user_email).to eq user1.email }
  it { expect(video.video_info.title).to eq "Ý thức bảo vệ chủ quyền biển đảo của dân tộc | VTV24" }

  context 'when url blank' do
    let(:video_blank) { build(:video, user_id: user1.id, url: '') }
    before do
      video_blank.valid?
    end
    subject { video_blank.errors.full_messages.first }
    it { is_expected.to eq "Url can't be blank" }
  end

  context 'when url invalid' do
    let(:video_blank) { build(:video, user_id: user1.id, url: 'http://localhost:3000') }
    before do
      video_blank.valid?
    end
    subject { video_blank.errors.full_messages.first }
    it { is_expected.to eq "Please enter the correct youtube link" }
  end

  context 'when url valid' do
    let(:video_blank) { build(:video, user_id: user1.id, url: 'https://www.youtube.com/watch?v=FbsO3bgyH84&ab_channel=catholiclofi') }
    before do
      video_blank.valid?
    end
    subject { video_blank.errors.full_messages }
    it { is_expected.not_to be_present }
  end
end
