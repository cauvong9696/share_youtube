require 'rails_helper'

describe User, type: :model do
  let(:user) { create(:user) }

  before do
    video1 = create(:video, user_id: user.id)
    video2 = create(:video, user_id: user.id)
    create(:vote, user_id: user.id, video_id: video1.id, type_vote: :up_vote)
    create(:vote, user_id: user.id, video_id: video2.id, type_vote: :down_vote)
  end

  it { expect(user.videos.size).to eq 2 }
  it { expect(user.votes.size).to eq 2 }
  it { expect(user.up_votes.size).to eq 1 }
  it { expect(user.up_votes.first.type_vote).to eq 'up_vote' }
  it { expect(user.down_votes.size).to eq 1 }
  it { expect(user.down_votes.first.type_vote).to eq 'down_vote' }
end
