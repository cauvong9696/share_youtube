class CreateVotes < ActiveRecord::Migration[7.0]
  def change
    create_table :votes do |t|
      t.references :user, null: false, foreign_key: true
      t.references :video, null: false, foreign_key: true
      t.bigint :type_vote, null:false, comment: '1: up vote, 2: down vote'

      t.timestamps
    end
  end
end
