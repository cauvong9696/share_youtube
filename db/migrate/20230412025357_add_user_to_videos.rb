class AddUserToVideos < ActiveRecord::Migration[7.0]
  def change
    add_reference :videos, :user, null: false, foreign_key: true, default: 1
  end
end
