class Video < ApplicationRecord
  belongs_to :user
  has_many :votes
  has_many :up_votes, -> { where(type_vote: :up_vote) }, class_name: "Vote"
  has_many :down_votes, -> { where(type_vote: :down_vote) }, class_name: "Vote"

  delegate :email, to: :user, prefix: true

  validates :url, presence: true, uniqueness: true
  validate :valid_url?

  VideoInfo.provider_api_keys = { youtube: ENV['YOUTUBE_API_KEY'] }

  def valid_url?
    return if url.blank? || VideoInfo.valid_url?(url)

    errors.add(:base, "Please enter the correct youtube link")
  end

  def video_info
    VideoInfo.new(url)
  end
end
