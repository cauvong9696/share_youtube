class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :videos
  has_many :votes
  has_many :up_votes, -> { where(type_vote: :up_vote) }, class_name: "Vote"
  has_many :down_votes, -> { where(type_vote: :down_vote) }, class_name: "Vote"
end
