class Vote < ApplicationRecord
  belongs_to :user
  belongs_to :video

  enum type_vote: { up_vote: 1, down_vote: 2 }
end
