module ApplicationHelper
  def flash_class flash_type
    case flash_type.to_sym
    when :success
      'bg-green-100 border-green-400 text-green-700'
    when :error
      'bg-red-100 border-red-400 text-red-700'
    else
      flash_type.to_s
    end
  end

  def error_list messages
    content_tag :ul do
      messages.map {|item| content_tag(:li, item) }.join.html_safe
    end
  end
end
