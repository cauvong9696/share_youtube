class VotesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_video

  def up_vote
    if @vote.persisted? && @vote.type_vote == "up_vote"
      @vote.destroy
    else
      @vote.type_vote = :up_vote
      @vote.save
    end
    find_vote_number
    respond_to do |format|
      format.js
    end
  end

  def down_vote
    if @vote.persisted? && @vote.type_vote == "down_vote"
      @vote.destroy
    else
      @vote.type_vote = :down_vote
      @vote.save
    end
    find_vote_number
    respond_to do |format|
      format.js
    end
  end

  private

  def find_video
    @video = Video.find_by(id: params[:id])
    @vote = Vote.find_or_initialize_by(user_id: current_user.id, video_id: params[:id])
  end

  def find_vote_number
    @video_up_vote_ids = current_user.up_votes.pluck(:video_id)
    @video_down_vote_ids = current_user.down_votes.pluck(:video_id)
  end
end
