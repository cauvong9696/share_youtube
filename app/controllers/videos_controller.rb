class VideosController < ApplicationController
  before_action :authenticate_user!, only: %i[new create]

  def index
    @videos = Video.order(created_at: :desc)
    if user_signed_in?
      @video_up_vote_ids = current_user.up_votes.pluck(:video_id)
      @video_down_vote_ids = current_user.down_votes.pluck(:video_id)
    end
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new(video_params)
    if @video.save
      flash[:success] = "Share success"
      redirect_to root_url
    else
      flash.now[:error] = @video.errors.full_messages
      render :new
    end
  end

  private

  def video_params
    params.require(:video).permit(:url).merge(user_id: current_user.id)
  end
end
